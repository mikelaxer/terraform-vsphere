provider "vsphere" {
    user = var.vsphere_user
    password = var.vsphere_password
    vsphere_server = var.viserver
    allow_unverified_ssl = true
}

provider "dyn" {
    customer_name = var.customer_name
    username = var.dyn_user
    password = var.dyn_password
}


data "vsphere_datacenter" "dc" {
  name = var.vmdc
}

data "vsphere_datastore" "datastore" {
  name = var.vmdatastore
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_compute_cluster" "cluster" {
  name          = var.vmcluster
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_resource_pool" "pool" {
  name = var.vmresourcepool[var.vmrp]
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_network" "network" {
  name = var.vmnetlabel[var.vmdomain]
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_virtual_machine" "template" {
  name          = var.vmtemp
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

resource "dyn_record" "tftest_cname1" {
  zone = "dev.int.nsidc.org"
  name = "dev.vmtfleader.mlaxer"
  value = "vmslushypack.dev.int.nsidc.org."
  type = "CNAME"
  ttl = "86400"
}

resource "dyn_record" "tftest_cname2" {
  zone = "dev.int.nsidc.org"
  name = "dev.vmtfworker1.mlaxer"
  value = "vmslushypenguin.dev.int.nsidc.org."
  type = "CNAME"
  ttl = "86400"
}

resource "dyn_record" "tftest_cname3" {
  zone = "dev.int.nsidc.org"
  name = "dev.vmtfworker2.mlaxer"
  value = "vmslushypermafrost.dev.int.nsidc.org."
  type = "CNAME"
  ttl = "86400"
}

resource "vsphere_virtual_machine" "vm1" {
  name             = var.vmname["vm1"]
  resource_pool_id = "${data.vsphere_compute_cluster.cluster.resource_pool_id}"
  folder = "Linux/SCM/dev"


  num_cpus         = 2
  cpu_share_level = "normal"
  memory           = 2048
  memory_share_level = "normal"
  guest_id = "ubuntu64Guest"


  wait_for_guest_net_timeout = 1


  network_interface {
    network_id   = "${data.vsphere_network.network.id}"
  }

  disk {
    label = "disk0"
    size  = 20
    datastore_id = "${data.vsphere_datastore.datastore.id}"
    thin_provisioned = "true"
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"

    customize {
      linux_options {
        host_name = "vmslushypack"
        domain = var.vmdomain
      }

      network_interface {
        ipv4_address = "172.18.243.236"
        ipv4_netmask = 21
      }

      ipv4_gateway = var.vmgateway[var.vmdomain]
      dns_server_list = var.vmdns
    }
  }
}
resource "vsphere_virtual_machine" "vm2" {
  name             = var.vmname["vm2"]
  resource_pool_id = "${data.vsphere_compute_cluster.cluster.resource_pool_id}"
  folder = "Linux/SCM/dev"


  num_cpus         = 2
  cpu_share_level = "normal"
  memory           = 2048
  memory_share_level = "normal"
  guest_id = "ubuntu64Guest"


  wait_for_guest_net_timeout = 1


  network_interface {
    network_id   = "${data.vsphere_network.network.id}"
  }

  disk {
    label = "disk0"
    size  = 20
    datastore_id = "${data.vsphere_datastore.datastore.id}"
    thin_provisioned = "true"
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"

    customize {
      linux_options {
        host_name = "vmslushypenguin"
        domain = var.vmdomain
      }

      network_interface {
        ipv4_address = "172.18.243.235"
        ipv4_netmask = 21
      }

      ipv4_gateway = var.vmgateway[var.vmdomain]
      dns_server_list = var.vmdns
    }
  }
}
resource "vsphere_virtual_machine" "vm3" {
  name             = var.vmname["vm3"]
  resource_pool_id = "${data.vsphere_compute_cluster.cluster.resource_pool_id}"
  folder = "Linux/SCM/dev"


  num_cpus         = 2
  cpu_share_level = "normal"
  memory           = 2048
  memory_share_level = "normal"
  guest_id = "ubuntu64Guest"


  wait_for_guest_net_timeout = 1


  network_interface {
    network_id   = "${data.vsphere_network.network.id}"
  }

  disk {
    label = "disk0"
    size  = 20
    datastore_id = "${data.vsphere_datastore.datastore.id}"
    thin_provisioned = "true"
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"

    customize {
      linux_options {
        host_name = "vmslushypermafrost"
        domain = var.vmdomain
      }

      network_interface {
        ipv4_address = "172.18.243.234"
        ipv4_netmask = 21
      }

      ipv4_gateway = var.vmgateway[var.vmdomain]
      dns_server_list = var.vmdns
    }
  }
}
