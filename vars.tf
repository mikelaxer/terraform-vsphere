
variable "vsphere_user" {}
variable "vsphere_password" {}

variable "vmname" {
  description = "Name of VM in vSphere"
  type = "map"
  default = {
    "vm1" = "mlaxer-tfcluster-leader"
    "vm2" = "mlaxer-tfcluster-worker1"
    "vm3" = "mlaxer-tfcluster-worker2"
  }
}

variable "viserver" {
  description = "vSphere server name"
  default = "iceshelf.ad.int.nsidc.org"
}

variable "vmdc" {
  description = "Data center name"
  default = "NSIDC"
}

variable "vmcluster" {
  description = "Cluster name"
  default = "Arctic"
}

variable "vmhostname" {
  description = "Default VM hostname"
  default = "test-host"
}

variable "vmrp" {
  description = "Default resource pool"
  default = "dev"
}

variable "vmdomain" {
  description = "Default domain"
  default = "dev"
}

variable "vmdatastore" {
  description = "Default datastore"
  default = "nsidc-silver-05"
}

variable "vmtemp" {
  description = "Default VM template"
  default = "xenial64-nsidc"
}

variable "vmdscluster" {
  description = "Map of datastores to datastore clusters"
  type = "map"
  default = {
    nsidc-silver-05 = "Silver"
    nsidc-silver-06 = "Silver"
    nsidc-silver-07 = "Silver"
    nsidc-silver-08 = "Silver"
    nsidc-silver-09 = "Silver"
    nsidc-silver-10 = "Silver"
    nsidc-silver-11 = "Silver"
    nsidc-gold-01 = "Gold"
    nsidc-gold-02 = "Gold"
    nsidc-gold-03 = "Gold"
    nsidc-gold-04 = "Gold"
    nsidc-gold-05 = "Gold"
  }
}

variable "vmresourcepool" {
  description = "Map of compute clusters"
  type = "map"
  default = {
    dev = "Non-Production"
    apps = "Production"
  }
}


variable "vmdns" {
  description = "DNS Servers"
  default = ["128.138.130.30","128.138.129.76","128.138.240.1"]
}

variable "vmgateway" {
  description = "Map of default gateways"
  type = "map"
  default = {
    dev = "172.18.240.1"
    apps = "172.18.248.1"
  }
}

variable "vmnetlabel" {
  description = "Map of VM network labels"
  type = "map"
  default = {
    dev = "DEV-10 172.18.225"
    apps = "APP-10 172.18.226"
  }
}

// Dyn variables
variable "dyn_user" {}
variable "dyn_password" {}

variable "customer_name" {
  default = "nsidc"
}

// These can probably be removed
variable "vmaddrbase" {
  type = "map"
  default = {
    dev = "172.18.244."
    apps = "172.18.251."
  }
}

variable "vmaddroctet" {
  default = "200"
}
